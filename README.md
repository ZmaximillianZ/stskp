приложение дневника тренировок
=================

Установка Docker
----------------
[Инструкция](https://docs.docker.com/install/linux/docker-ce/ubuntu/#set-up-the-repository) для установки Docker CE

Чтобы добавить репозиторий на Linux Mint используйте команду:
```
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable"
```

Установка Docker Compose
------------------------
[Инструкция](https://docs.docker.com/compose/install/#install-compose) для установки Docker compose

Добавить текущего пользовать в группу docker, для работы без sudo (заработает после новой авторизации)
------------------------
```
sudo usermod -aG docker $USER
```



Установка PHP
--------------
Включите PPA (Персональные архивы пакетов)
```
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
```
Установите наиболее часто используемые модули, не хватает модулей для постгреса
```
sudo apt-get install php7.4-cli php7.4-common php7.4-fpm php7.4-json php7.4-opcache php7.4-mbstring \
    php7.4-zip php7.4-curl php7.4-xml php7.4-soap php7.4-gd php7.4-imap php7.4-intl php7.4-bcmath php-redis \
    php-amqp php7.4-phpdbg
```

Установка Composer
------------------
```
sudo php -r "\$downloaded = copy('https://getcomposer.org/installer', 'composer-setup.php');if (\$downloaded === false) {exit('Can\'t download composer-setup.php');}if ((\$installerSig = trim(file_get_contents('https://composer.github.io/installer.sig')))  === false) {exit('Can\'t download installer.sig');}if (hash_file('SHA384', 'composer-setup.php') === \$installerSig) {echo 'Installer verified';exec('sudo php composer-setup.php --install-dir=/usr/bin --filename=composer');} else {echo 'Installer corrupt';}echo PHP_EOL;unlink('composer-setup.php');"
```
###Prestissimo (плагин для Composer)
```
composer global require hirak/prestissimo
```

###Генерация нового SSH ключа
```
ssh-keygen -t rsa -C "your_email@example.com"
```

###Скопируйте ssh key и добавьте его в ваш профиль [здесь](https://git.stforex.net/plugins/servlet/ssh/account/keys)
```
cat ~/.ssh/id_rsa.pub
```

Установка Гита (Git)
-------------------
```
sudo apt-get install git
```

###Запуск Docker

Перед запуском не забудьте прописать в /etc/hosts 
train.log 127.0.0.1, иначе при выполнении sh scripts/docker-start.sh получите ошибку. 

###Установка Composer (Install package)
```
composer i --optimize-autoloader
```
###Запуск миграций
```
sh scripts/migrate.sh
```

Конфигурация и настройка Xdebug
===============================
 В ``.env.local`` файле нужно ввести 
```
XDEBUG_ENABLE=1
XDEBUG_CONFIG=idekey=PHPSTORM remote_host=host.docker.internal remote_enable=1 remote_connect_back=1 remote_autostart=1 profiler_enable=0 remote_port=9666
```
 В PhpStorm->Settings->Languages & Frameworks->PHP->Debug в разделе Xdebug нужно ввести 
```
Debug port - 9666
Force break at first line when no path mapping specified: inactive
Force break at first line when a script is outside the project: active  
```    
 В PhpStorm->Settings->Languages & Frameworks->PHP->Debug->DBGp Proxy нужно ввести 
 ```
 IDE key: PHPSTORM
 Host: host.docker.internal
 Port: 9666
 ```
Для удаленного дебага нужно создать файл, открыть PhpStorm->menu->Run->Edit Configurations, нажать кнопку Плюс, выбрать PHP Remote Debug,
```
Name: xdebug
(see below) Server: docker fpm 
IDE key (session id): PHPSTORM
Filter debug connection by IDE key: inactive
```
Чтобы сконфигурировать docker fpm кликните на ``Filter debug connection by IDE key`` флажок, кликните на ``...`` в строке Server,
В открывшемся новом окне 'Servers' нажмите кнопку Плюс и введите:   
```
Name: docker fpm
Host: 172.22.0.0 - под вопросом
Port: 9666
Debugger: Xdebug
use pat mapping: active
```
Для удаленного дебага в Google Chrome нужно скачать расширение ``Xdebug helper``  и сконфигурировать его 
```
IDE key
Select your IDE to use the default sessionkey or choose other to use a custom key.
PhpStorm  PHPSTORM
```
Использование:
 * выберите xdebug Run file
 * кликните на debug кнопку (зеленый паучок)
 * кликните на телефонную трубку (Start listening for PHP Debug Connections)
 
Конфигурация Интерпретатора в PhpStorm
======================================
В PhpStorm->Settings->Languages & Frameworks->PHP
```
PHP Languages: 7.4
CLI Interpreter: PHP 7.4
```
Чтобы сконфигурировать CLI Interpreter нажмите на ``...``, нажмите на кнопку Плюс в открывшемся окне 'CLI Interpreter' и введите  
```
Name: PHP 7.4
PHP Executable: /usr/bin/php7.4
```

Плагины PhpStorm
================
* [php toolbox](https://plugins.jetbrains.com/plugin/8133-php-toolbox)
* [php inspections (ea extended)](https://plugins.jetbrains.com/plugin/7622-php-inspections-ea-extended-)
* [symfony-plugin](https://plugins.jetbrains.com/plugin/7219-symfony-plugin)
* [PHPUnit Enhancement] (https://plugins.jetbrains.com/plugin/9674-phpunit-enhancement)
  
Настройка Symfony Plugin для PhpStorm
==========================================
![Настройки](../images/plugin.jpg) - забрать картинку с трейдер аккаунта
  
Конфигурация Code sniffer
=========================
Можно установить Composer package для PHP Code Sniffer. Его можно использовать для того, чтобы установка проходила в рамках всей системы. Команда:
```
composer global require "squizlabs/php_codesniffer=*"
```
В PhpStorm->Settings->Languages & Frameworks->PHP->PHP Code Sniffer

Возле 'Confuguration' кликните на ``...``, кликните на Плюс кнопку и в открывшемся окне 'Code Sniffer' и введите   
```
Php Code Sniffer phpcs: ~/.composer/vendor/squizlabs/php_codesniffer/bin/phpcs
```
В PhpStorm->Editor->Inspections->PHP Code Sniffer validation    configure
```
show warnings as: Warning 
Show sniff name: active
Coding standard: PSR2 
```

Конфигурация тестового окружения 
================================
В PhpStorm->Settings->Languages & Frameworks->PHP->Test Frameworks нажмите кнопку Плюс и введите 
```
use composer autoloader: true
path to script: ~/projects/stskp/vendor/autoload.php 
```
откройте PhpStorm->menu->Run->Edit Configurations, нажмите кнопку Плюс, выберите PHP Unit и введите 
```
name: tests
Test scope: Defined in the configuration file
use alternative configuration file: active, ~/projects/stskp/phpunit.xml
```
Перенастройка цвета лога
========================
(По умолчанию серый на розовом фоне)

File - Settings - Editor - Color Scheme - Console Colors - Ansi Colors