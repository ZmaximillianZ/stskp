<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200309171905 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE training_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE main_user_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE person_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE admin_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE training (id INT NOT NULL, person_id INT DEFAULT NULL, health INT NOT NULL, training_type INT NOT NULL, code VARCHAR(255) NOT NULL, date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, description VARCHAR(4096) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D5128A8F217BBB47 ON training (person_id)');
        $this->addSql('CREATE TABLE main_user (id INT NOT NULL, person_id INT DEFAULT NULL, auth_key VARCHAR(32) NOT NULL, password_hash VARCHAR(255) NOT NULL, password_reset_token VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, ip VARCHAR(45) DEFAULT NULL, user_name VARCHAR(32) NOT NULL, first_name VARCHAR(32) NOT NULL, last_name VARCHAR(32) NOT NULL, middle_name VARCHAR(32) DEFAULT NULL, email VARCHAR(255) NOT NULL, status INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6D20E42B217BBB47 ON main_user (person_id)');
        $this->addSql('CREATE TABLE person (id INT NOT NULL, city_id INT NOT NULL, social_accounts VARCHAR(5120) DEFAULT NULL, body_type INT DEFAULT NULL, personal_life VARCHAR(2048) DEFAULT NULL, birth_day TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, about VARCHAR(4096) DEFAULT NULL, phone VARCHAR(32) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE person_training (person_id INT NOT NULL, training_id INT NOT NULL, PRIMARY KEY(person_id, training_id))');
        $this->addSql('CREATE INDEX IDX_41542FB8217BBB47 ON person_training (person_id)');
        $this->addSql('CREATE INDEX IDX_41542FB8BEFD98D1 ON person_training (training_id)');
        $this->addSql('CREATE TABLE admin (id INT NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled BOOLEAN NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, roles TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_880E0D7692FC23A8 ON admin (username_canonical)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_880E0D76A0D96FBF ON admin (email_canonical)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_880E0D76C05FB297 ON admin (confirmation_token)');
        $this->addSql('COMMENT ON COLUMN admin.roles IS \'(DC2Type:array)\'');
        $this->addSql('ALTER TABLE training ADD CONSTRAINT FK_D5128A8F217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE main_user ADD CONSTRAINT FK_6D20E42B217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE person_training ADD CONSTRAINT FK_41542FB8217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE person_training ADD CONSTRAINT FK_41542FB8BEFD98D1 FOREIGN KEY (training_id) REFERENCES training (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE person_training DROP CONSTRAINT FK_41542FB8BEFD98D1');
        $this->addSql('ALTER TABLE training DROP CONSTRAINT FK_D5128A8F217BBB47');
        $this->addSql('ALTER TABLE main_user DROP CONSTRAINT FK_6D20E42B217BBB47');
        $this->addSql('ALTER TABLE person_training DROP CONSTRAINT FK_41542FB8217BBB47');
        $this->addSql('DROP SEQUENCE training_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE main_user_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE person_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE admin_id_seq CASCADE');
        $this->addSql('DROP TABLE training');
        $this->addSql('DROP TABLE main_user');
        $this->addSql('DROP TABLE person');
        $this->addSql('DROP TABLE person_training');
        $this->addSql('DROP TABLE admin');
    }
}
