<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20200309185202.
 */
final class Version20200309185202 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE result_field_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE result_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE training_result_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE field_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE result_field (id INT NOT NULL, value NUMERIC(10, 0) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, trainingResult_id INT DEFAULT NULL, fieldType_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_CA0112D44CF36217 ON result_field (trainingResult_id)');
        $this->addSql('CREATE INDEX IDX_CA0112D4D986DE8E ON result_field (fieldType_id)');
        $this->addSql('CREATE TABLE result_type (id INT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(2048) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, trainingResult_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1CE54C674CF36217 ON result_type (trainingResult_id)');
        $this->addSql('CREATE TABLE training_result (id INT NOT NULL, training_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2086067CBEFD98D1 ON training_result (training_id)');
        $this->addSql('CREATE TABLE field_type (id INT NOT NULL, type INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, resultType_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9F123E93175D69D0 ON field_type (resultType_id)');
        $this->addSql('ALTER TABLE result_field ADD CONSTRAINT FK_CA0112D44CF36217 FOREIGN KEY (trainingResult_id) REFERENCES training_result (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE result_field ADD CONSTRAINT FK_CA0112D4D986DE8E FOREIGN KEY (fieldType_id) REFERENCES field_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE result_type ADD CONSTRAINT FK_1CE54C674CF36217 FOREIGN KEY (trainingResult_id) REFERENCES training_result (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE training_result ADD CONSTRAINT FK_2086067CBEFD98D1 FOREIGN KEY (training_id) REFERENCES training (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE field_type ADD CONSTRAINT FK_9F123E93175D69D0 FOREIGN KEY (resultType_id) REFERENCES result_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE field_type DROP CONSTRAINT FK_9F123E93175D69D0');
        $this->addSql('ALTER TABLE result_field DROP CONSTRAINT FK_CA0112D44CF36217');
        $this->addSql('ALTER TABLE result_type DROP CONSTRAINT FK_1CE54C674CF36217');
        $this->addSql('ALTER TABLE result_field DROP CONSTRAINT FK_CA0112D4D986DE8E');
        $this->addSql('DROP SEQUENCE result_field_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE result_type_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE training_result_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE field_type_id_seq CASCADE');
        $this->addSql('CREATE SEQUENCE hdb_catalog.remote_schemas_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('DROP TABLE result_field');
        $this->addSql('DROP TABLE result_type');
        $this->addSql('DROP TABLE training_result');
        $this->addSql('DROP TABLE field_type');
    }
}
