<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\FieldType;
use App\Entity\ResultField;
use App\Entity\TrainingResult;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ResultFieldFixtures extends AbstractFixtures implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < static::BATCH; ++$i) {
            $resultField = new ResultField();
            /** @var FieldType $fieldType */
            $fieldType = $this->getReference('fieldType' . $i);
            /** @var TrainingResult $trainingResult */
            $trainingResult = $this->getReference('trainingResult' . $i);
            $resultField
                ->setTrainingResult($trainingResult)
                ->setFieldType($fieldType)
                ->setValue(rand(0, 10) / 10);
            $manager->persist($resultField);
            $this->addReference('resultField' . $i, $resultField);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            FieldTypeFixtures::class,
            TrainingResultFixtures::class,
        ];
    }
}
