<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Admin;
use Doctrine\Persistence\ObjectManager;

class AdminFixtures extends AbstractFixtures
{
    public function load(ObjectManager $manager): void
    {
        $user = (new Admin())
            ->setUsername('admin')
            ->setEnabled(true)
            ->setEmail('max@mail.com')
            ->setPlainPassword('max123')
            ->addRole('ROLE_ADMIN');

        $manager->persist($user);
        $manager->flush();
    }
}
