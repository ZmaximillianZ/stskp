<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Training;
use App\Entity\TrainingResult;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class TrainingResultFixtures extends AbstractFixtures implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < static::BATCH; ++$i) {
            $trainingResult = new TrainingResult();
            /** @var Training $training */
            $training = $this->getReference('training' . $i);
            $trainingResult
                ->setTraining($training);
            $manager->persist($trainingResult);
            $this->addReference('trainingResult' . $i, $trainingResult);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            TrainingFixtures::class,
        ];
    }
}
