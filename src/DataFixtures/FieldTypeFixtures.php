<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\FieldType;
use App\Entity\ResultType;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class FieldTypeFixtures extends AbstractFixtures implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < static::BATCH; ++$i) {
            $fieldType = new FieldType();
            /** @var ResultType $resultType */
            $resultType = $this->getReference('resultType' . $i);
            $fieldType
                ->setResultType($resultType)
                ->setType(rand(1, 5));
            $manager->persist($fieldType);
            $this->addReference('fieldType' . $i, $fieldType);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            ResultTypeFixtures::class,
        ];
    }
}
