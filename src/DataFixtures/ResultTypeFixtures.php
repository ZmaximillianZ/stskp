<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\ResultType;
use App\Entity\TrainingResult;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ResultTypeFixtures extends AbstractFixtures implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < static::BATCH; ++$i) {
            $resultType = new ResultType();
            /** @var TrainingResult $trainingResult */
            $trainingResult = $this->getReference('trainingResult' . $i);
            $resultType
                ->setName(static::generateRandomString(10))
                ->setDescription(static::generateRandomString(30))
                ->setTrainingResult($trainingResult);
            $manager->persist($resultType);
            $this->addReference('resultType' . $i, $resultType);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
           TrainingResultFixtures::class,
       ];
    }
}
