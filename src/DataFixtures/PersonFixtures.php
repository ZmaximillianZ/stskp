<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Person;
use App\Entity\Training;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PersonFixtures extends AbstractFixtures implements DependentFixtureInterface
{
    /**
     * @throws \Exception
     */
    public function load(ObjectManager $manager): void
    {
        $person = new Person();
        $birthDay = new \DateTime('now -20 year');
        $person
            ->setAbout(static::generateRandomString(200))
            ->setBirthDay($birthDay)
            ->setPersonalLife(static::generateRandomString(200))
            ->setSocialAccounts(static::generateRandomString(200))
            ->setBodyType(array_rand(Person::BODY_TYPE))
            ->setCityId(101)
            ->setPhone('+375292775803');
        $manager->persist($person);
        $this->addReference('person', $person);
        for ($i = 0; $i < static::BATCH; ++$i) {
            $person = new Person();
            /** @var Training $training */
            $training = $this->getReference('training' . $i);
            $birthDay = new \DateTime('now -20 year');
            $person
                ->setAbout(static::generateRandomString(200))
                ->setBirthDay($birthDay)
                ->setPersonalLife(static::generateRandomString(200))
                ->setSocialAccounts(static::generateRandomString(200))
                ->setBodyType(array_rand(Person::BODY_TYPE))
                ->setCityId($i)
                ->setPhone('+3752912345' . $i)
                ->addTraining($training);
            $manager->persist($person);
            $this->addReference('person' . $i, $person);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            TrainingFixtures::class,
        ];
    }
}
