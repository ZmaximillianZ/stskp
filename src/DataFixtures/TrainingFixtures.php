<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Training;
use Doctrine\Persistence\ObjectManager;

class TrainingFixtures extends AbstractFixtures
{
    /**
     * @throws \Exception
     */
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < static::BATCH; ++$i) {
            $training = new Training();
            $training
                ->setDescription(static::generateRandomString(30))
                ->setCode(static::generateRandomString(20))
                ->setDate(new \DateTime('now -' . $i . ' day'))
                ->setHealth(array_rand(Training::HEALTH))
                ->setTrainingType(array_rand(Training::TRAINING_TYPE));
            $manager->persist($training);
            $this->addReference('training' . $i, $training);
        }

        $manager->flush();
    }
}
