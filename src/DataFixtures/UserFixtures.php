<?php

namespace App\DataFixtures;

use App\Entity\Person;
use App\Entity\User;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends AbstractFixtures implements DependentFixtureInterface
{
    private UserPasswordEncoderInterface $passwordEncoder;

    public const USER_NAME = 'max';

    public const NAME = 'Maxim';

    public const LAST_NAME = 'Hryshyn';

    public const IP = '127.0.0.1';

    public const EMAIL = 'mail.com';

    public const PASSWORD = 'max123';

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager): void
    {
        $authKey = str_shuffle('sadf98f343jd89394j49');
        $user = new User();
        /** @var Person $person */
        $person = $this->getReference('person');
        $user
            ->setAuthKey($authKey)
            ->setPasswordHash($this->passwordEncoder->encodePassword($user, static::PASSWORD))
            ->setUserName(static::USER_NAME)
            ->setFirstName(static::NAME)
            ->setLastName(static::LAST_NAME)
            ->setPerson($person)
            ->setIp(static::IP)
            ->setEmail(static::EMAIL)
            ->setStatus(User::ACTIVE);
        $manager->persist($user);
        for ($i = 0; $i < static::BATCH; ++$i) {
            $user1 = new User();
            /** @var Person $person */
            $person = $this->getReference('person' . $i);
            $user1
                ->setAuthKey($authKey)
                ->setPasswordHash($this->passwordEncoder->encodePassword($user1, static::PASSWORD))
                ->setUserName(static::generateRandomString())
                ->setFirstName(static::generateRandomString())
                ->setLastName(static::generateRandomString())
                ->setPerson($person)
                ->setIp(static::IP)
                ->setEmail(static::generateRandomString(8) . '@gmail.com')
                ->setStatus(array_rand(User::USER_STATUS));
            $manager->persist($user1);
        }
        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            PersonFixtures::class,
        ];
    }
}
