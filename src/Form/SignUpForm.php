<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class SignUpForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('user_name', TextType::class, ['label' => 'username'])
            ->add('first_name', TextType::class, ['label' => 'first name'])
            ->add('last_name', TextType::class, ['label' => 'last name'])
            ->add('email', TextType::class, ['label' => 'email'])
            ->add('password', TextType::class, ['label' => 'password'])
            ->add('save', SubmitType::class, ['label' => 'save']);
    }
}
