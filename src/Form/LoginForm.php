<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class LoginForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('user_name', null, ['label' => 'username'])
            ->add('password', null, ['label' => 'password'])
            ->add('login', SubmitType::class, ['label' => 'Login']);
    }
}
