<?php

declare(strict_types=1);

namespace App\Controller;

use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManager;

class AdminController extends EasyAdminController
{
    /**
     * @return mixed
     */
    public function createNewUserEntity()
    {
        /** @var UserManager $userManager */
        $userManager = $this->get('fos_user.user_manager');

        return $userManager->createUser();
    }

    /**
     * @param UserInterface $user
     */
    public function persistUserEntity($user): void
    {
        /** @var UserManager $userManager */
        $userManager = $this->get('fos_user.user_manager');
        $userManager->updateUser($user);

        parent::persistEntity($user);
    }

    /**
     * @param UserInterface $user
     */
    public function updateUserEntity($user): void
    {
        /** @var UserManager $userManager */
        $userManager = $this->get('fos_user.user_manager');
        $userManager->updateUser($user);
        parent::updateEntity($user);
    }
}
