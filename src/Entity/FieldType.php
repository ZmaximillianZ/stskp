<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ResultFieldRepository")
 * @ORM\Table(name="field_type")
 * @ORM\HasLifecycleCallbacks
 */
class FieldType
{
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="type", type="integer", nullable=false)
     */
    private $type;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true, options={"default"="NULL"})
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true, options={"default"="NULL"})
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="ResultType", inversedBy="fieldTypes")
     */
    protected $resultType;

    /**
     * @ORM\OneToMany(targetEntity="ResultField", mappedBy="fieldType")
     */
    protected $resultFields;

    public function getResultFields(): ?Collection
    {
        return $this->resultFields;
    }

    public function addResultField(?ResultField $resultField): self
    {
        if (!$this->resultFields->contains($resultField)) {
            $this->resultFields[] = $resultField;
        }

        return $this;
    }

    public function removeResultField(ResultField $resultField): self
    {
        if ($this->resultFields->contains($resultField)) {
            $this->resultFields->remove($resultField);
            $resultField->setFieldType(null);
        }

        return $this;
    }

    public function getResultType(): ?ResultType
    {
        return $this->resultType;
    }

    public function setResultType(?ResultType $resultType): self
    {
        $this->resultType = $resultType;

        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist(): void
    {
        $this->createdAt = new \DateTime('now');
    }

    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate(): void
    {
        $this->updatedAt = new \DateTime('now');
    }
}
