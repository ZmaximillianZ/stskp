<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PersonRepository")
 * @ORM\Table(name="person")
 * @ORM\HasLifecycleCallbacks
 */
class Person
{
    public const ECTOMORPH = 0;

    public const MESOMORPH = 1;

    public const ENDOMORPH = 2;

    public const BODY_TYPE = [
        self::ECTOMORPH,
        self::MESOMORPH,
        self::ENDOMORPH,
    ];

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="city_id", type="integer", length=6, nullable=false)
     */
    private $cityId;

    /**
     * @ORM\Column(name="social_accounts", type="string", length=5120, nullable=true, options={"default"="NULL"}))
     */
    private $socialAccounts;

    /**
     * @ORM\Column(name="body_type", type="integer", length=6, nullable=true, options={"default"="NULL"})
     */
    private $bodyType;

    /**
     * @ORM\Column(name="personal_life", type="string", length=2048, nullable=true, options={"default"="NULL"})
     */
    private $personalLife;

    /**
     * @ORM\Column(name="birth_day", type="datetime", nullable=true, options={"default"="NULL"})
     */
    private $birthDay;

    /**
     * @ORM\Column(name="about", type="string", length=4096, nullable=true, options={"default"="NULL"})
     */
    private $about;

    /**
     * @ORM\Column(name="phone", type="string", nullable=true, length=32, options={"default"="NULL"})
     */
    private $phone;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true, options={"default"="NULL"})
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true, options={"default"="NULL"})
     */
    private $updatedAt;

    /**
     * @ORM\OneToOne(targetEntity="User", mappedBy="person")
     */
    protected $user;

    /**
     * @ORM\ManyToMany(targetEntity="Training", cascade={"persist"})
     * @ORM\JoinTable(
     *      name="person_training",
     *      joinColumns={@ORM\JoinColumn(name="person_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="training_id", referencedColumnName="id")},
     * )
     */
    protected $trainings;

    public function __construct()
    {
        $this->trainings = new ArrayCollection();
    }

    public function getTrainings(): Collection
    {
        return $this->trainings;
    }

    public function addTraining(Training $training): self
    {
        if (!$this->trainings->contains($training)) {
            $this->trainings[] = $training;
            $training->setPerson($this);
        }

        return $this;
    }

    public function removeTraining(Training $training): self
    {
        if ($this->trainings->contains($training)) {
            $this->trainings->removeElement($training);
            $training->setPerson(null);
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getCityId(): int
    {
        return $this->cityId;
    }

    public function setCityId(int $cityId): self
    {
        $this->cityId = $cityId;

        return $this;
    }

    public function getSocialAccounts(): ?string
    {
        return $this->socialAccounts;
    }

    public function setSocialAccounts(?string $socialAccounts): self
    {
        $this->socialAccounts = $socialAccounts;

        return $this;
    }

    public function getBodyType(): ?int
    {
        return $this->bodyType;
    }

    public function setBodyType(?int $bodyType): self
    {
        $this->bodyType = $bodyType;

        return $this;
    }

    public function getPersonalLife(): ?string
    {
        return $this->personalLife;
    }

    public function setPersonalLife(?string $personalLife): self
    {
        $this->personalLife = $personalLife;

        return $this;
    }

    public function getBirthDay(): ?\DateTime
    {
        return $this->birthDay;
    }

    public function setBirthDay(?\DateTime $birthDay): self
    {
        $this->birthDay = $birthDay;

        return $this;
    }

    public function getAbout(): ?string
    {
        return $this->about;
    }

    public function setAbout(?string $about): self
    {
        $this->about = $about;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist(): void
    {
        $this->createdAt = new \DateTime('now');
    }

    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate(): void
    {
        $this->updatedAt = new \DateTime('now');
    }
}
