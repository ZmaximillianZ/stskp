<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TrainingRepository")
 * @ORM\Table(name="training")
 * @ORM\HasLifecycleCallbacks
 */
class Training
{
    public const HEALTH_GOOD = 0;

    public const HEALTH_NORMAL = 1;

    public const HEALTH_BAD = 2;

    public const TRAINING_TYPE_COMMAND = 0;

    public const TRAINING_TYPE_PERSONAL = 1;

    public const TRAINING_TYPE_COMMAND_PERSONAL = 2;

    public const HEALTH = [
        self::HEALTH_GOOD,
        self::HEALTH_BAD,
        self::HEALTH_NORMAL,
    ];

    public const TRAINING_TYPE = [
        self::TRAINING_TYPE_COMMAND,
        self::TRAINING_TYPE_COMMAND_PERSONAL,
        self::TRAINING_TYPE_COMMAND_PERSONAL,
    ];

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="health", type="integer", length=6, nullable=false)
     */
    private $health;

    /**
     * @ORM\Column(name="training_type", type="integer", length=6, nullable=false)
     */
    private $trainingType;

    /**
     * @ORM\Column(name="code", type="string", nullable=false)
     */
    private $code;

    /**
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true, options={"default"="NULL"})
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true, options={"default"="NULL"})
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="description", type="string", length=4096, nullable=true, options={"default"="NULL"})
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="Person")
     */
    protected $person;

    /**
     * @ORM\OneToMany(targetEntity="TrainingResult", mappedBy="training")
     */
    protected $trainingResults;

    public function __toString()
    {
        return (string) $this->getId();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getPerson(): Person
    {
        return $this->person;
    }

    public function setPerson(Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getHealth(): int
    {
        return $this->health;
    }

    public function setHealth(int $health): self
    {
        $this->health = $health;

        return $this;
    }

    public function getTrainingType(): int
    {
        return $this->trainingType;
    }

    public function setTrainingType(int $trainingType): self
    {
        $this->trainingType = $trainingType;

        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTrainingResults(): ?Collection
    {
        return $this->trainingResults;
    }

    public function addResultType(TrainingResult $trainingResult): self
    {
        $this->trainingResults[] = $trainingResult;

        return $this;
    }

    public function removeResultType(TrainingResult $trainingResult): self
    {
        $this->trainingResults->remove($trainingResult);
        $trainingResult->setTraining(null);

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist(): void
    {
        $this->createdAt = new DateTime('now');
    }

    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate(): void
    {
        $this->updatedAt = new DateTime('now');
    }
}
