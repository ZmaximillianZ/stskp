<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ResultTypeRepository")
 * @ORM\Table(name="result_type")
 * @ORM\HasLifecycleCallbacks
 */
class ResultType
{
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(name="description", type="string", length=2048, nullable=true, options={"default"="NULL"})
     */
    private $description;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true, options={"default"="NULL"})
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true, options={"default"="NULL"})
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="FieldType", mappedBy="resultType")
     */
    protected $fieldTypes;

    /**
     * @ORM\ManyToOne(targetEntity="TrainingResult", inversedBy="resultType")
     */
    private $trainingResult;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getFieldTypes(): ?Collection
    {
        return $this->fieldTypes;
    }

    public function addFieldType(FieldType $fieldType): self
    {
        if (!$this->fieldTypes->contains($fieldType)) {
            $this->fieldTypes[] = $fieldType;
        }

        return $this;
    }

    public function removeFieldType(FieldType $fieldType): self
    {
        if ($this->fieldTypes->contains($fieldType)) {
            $this->fieldTypes->removeElement($fieldType);
            $fieldType->setResultType(null);
        }

        return $this;
    }

    public function getTrainingResult(): TrainingResult
    {
        return $this->trainingResult;
    }

    public function setTrainingResult(TrainingResult $trainingResult): self
    {
        $this->trainingResult = $trainingResult;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist(): void
    {
        $this->createdAt = new \DateTime('now');
    }

    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate(): void
    {
        $this->updatedAt = new \DateTime('now');
    }
}
