<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="training_result")
 */
class TrainingResult
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="ResultType", mappedBy="training")
     * !ORM\JoinColumn(onDelete="CASCADE", nullable=false)
     */
    private $resultType;

    /**
     * @ORM\ManyToOne(targetEntity="Training", inversedBy="trainingResults")
     */
    private $training;

    /**
     * @ORM\OneToMany(targetEntity="ResultField", mappedBy="trainingResult")
     */
    protected $resultFields;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getResultType(): ?Collection
    {
        return $this->resultType;
    }

    public function addResultType(ResultType $resultType): self
    {
        if (!$this->resultType->contains($resultType)) {
            $this->resultType[] = $resultType;
        }

        return $this;
    }

    public function removeResultType(ResultType $resultType): self
    {
        if ($this->resultType->contains($resultType)) {
            $this->resultType->remove($resultType);
            $resultType->setTrainingResult(null);
        }

        return $this;
    }

    public function getTraining(): ?Training
    {
        return $this->training;
    }

    public function setTraining(Training $training): self
    {
        $this->training = $training;

        return $this;
    }

    public function getResultFields(): ?ResultField
    {
        return $this->resultFields;
    }

    public function setResultFields(ResultField $resultFields): self
    {
        $this->resultFields = $resultFields;

        return $this;
    }
}
