#!/usr/bin/env bash

docker exec -ti $(docker ps -f name=stskp_fpm -q) sh -c "bin/console d:q:s 'drop schema if exists public cascade' && bin/console d:q:s 'create schema public'"
