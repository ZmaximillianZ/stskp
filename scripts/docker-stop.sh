#!/usr/bin/env bash

echo "docker rm stskp_fpm nginx_stskp stskp_db stskp_graphql_engine --force"

docker rm stskp_fpm nginx_stskp stskp_db stskp_graphql_engine --force