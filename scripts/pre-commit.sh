
cd .. && php vendor/bin/php-cs-fixer fix --config=.php_cs.dist -v --dry-run --stop-on-violation --using-cache=no && \
php vendor/bin/phpmnd src --non-zero-exit-on-violation && \
php vendor/bin/phpmnd tests --non-zero-exit-on-violation && \
php vendor/bin/phpstan analyse src && \
php vendor/bin/phpcpd src tests && \
./bin/phpunit && \
cd -

