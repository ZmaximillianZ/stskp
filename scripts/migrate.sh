#!/usr/bin/env bash

docker exec -ti $(docker ps -f name=stskp_fpm -q) sh -c "bin/console doctrine:migrations:migrate"
