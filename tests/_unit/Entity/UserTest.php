<?php

namespace tests\Entity;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    /**
     * @dataProvider nameDataProvider
     */
    public function testUser(string $name): void
    {
        $user = new User();
        $user->setFirstName($name);
        $this->assertEquals($name, $user->getFirstName());
    }

    public function nameDataProvider(): array
    {
        return [
            ['name' => 'Maxim'],
        ];
    }
}
